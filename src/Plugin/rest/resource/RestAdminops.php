<?php

namespace Drupal\drupal_admin_app\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource for database watchdog log entries.
 *
 * @RestResource(
 *   id = "dn_adminopa",
 *   label = @Translation("Drupal Admin App"),
 *   uri_paths = {
 *     "canonical" = "/dnadmin/{id}",
 *     "create" = "/rest/dnadmin/data"
 *   }
 * )
 */
class RestAdminops extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * Returns a watchdog log entry for the specified ID.
   *
   * @param int $id
   *   The ID of the watchdog log entry.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the log entry was not found.
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Thrown when no log entry was provided.
   */
  public function get($id = NULL) {
    if ($id) {
      $record = db_query("SELECT * FROM {watchdog} WHERE wid = :wid", [':wid' => $id])
        ->fetchAssoc();
      if (!empty($record)) {
        return new ResourceResponse($record);
      }

      throw new NotFoundHttpException('Log entry with ID @id was not found', ['@id' => $id]);
    }

    throw new BadRequestHttpException('No log entry ID was provided');
  }

  /**
   * Responds to POST requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data) {
    $result_arr = ['message' => '', 'status' => 'false', 'ops' => $data["ops"], 'roles' => ''];
    // =====
    // $result_arr['roles'] = [] ;
    // $roles = \Drupal::currentUser()->getRoles();
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $result_arr['roles'] = $roles;
    // =====
    $response = new ResourceResponse($result_arr);
    $response->addCacheableDependency($result_arr);
    return $response;
  }

}
